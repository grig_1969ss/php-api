<?php

class Database
{
    private $host = "localhost:8111";
    private $database_name = "php_api";
    private $username = "root";
    private $password = "";

    public $conn;

    /**
     * @return PDO|null
     */
    public function getConnection()
    {
        $this->conn = null;
        try {
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->database_name, $this->username, $this->password);
            $this->conn->exec("set names utf8");
        } catch (PDOException $exception) {
            echo "Database could not be connected: " . $exception->getMessage();
        }
        return $this->conn;
    }
}