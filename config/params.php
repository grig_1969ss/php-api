<?php

return [
    'serverName' => $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'],
    'uploadDirPath' => '/uploads/'
];
