<?php

/**
 * @return string
 */
function upload()
{
    $params = include_once '../config/params.php';

    if (!empty($_FILES)) {

        $tmp_name = $_FILES['banner']['tmp_name'];
        $ext = explode('.', $_FILES["banner"]["name"]);
        $ext = end($ext);
        $ext = strtolower($ext);
        $path = $params['uploadDirPath'] . time() . '_' . rand() . '.' . $ext;
        $path_uploads = '..' . $path;

        if (move_uploaded_file($tmp_name, $path_uploads)) {

            return $path;

        }

    }

    return false;
}

/**
 * @param $message
 */
function message($message)
{
    echo $message;
    exit;
}

/**
 * @param $amount
 * @return int
 */
function getCurrentAmountVal($amount)
{
    return intval($amount) - 1;
}

/**
 * @param $banner
 * @return string
 */
function getBannerLink($banner)
{
    $params = include_once '../config/params.php';

    return $params['serverName'] . $banner;
}

