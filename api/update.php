<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../config/database.php';
include_once '../class/posts.php';
include_once '../helper/functions.php';

if (!empty($_POST)) {
    $database = new Database();
    $db = $database->getConnection();

    $item = new Posts($db);

    $item->id = $_POST['id'];
    $item->text = $_POST['text'];
    $item->price = $_POST['price'];
    $item->amount = $_POST['amount'];
    $item->banner = upload();

    if ($item->updatePost()) {
        message('Post data updated.');
    } else {
        message('Data could not be updated');
    }
}
?>