<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");


include_once '../config/database.php';
include_once '../class/posts.php';
include_once '../helper/functions.php';
$params = include_once '../config/params.php';

$database = new Database();
$db = $database->getConnection();

$items = new Posts($db);

$stmt = $items->getPost();
$itemCount = $stmt->rowCount();

$postModel = $stmt->fetch(PDO::FETCH_ASSOC);

if (!$itemCount) {
    message('No record found');
}

$items->id = intval($postModel['id']);
$items->amount = getCurrentAmountVal($postModel['amount']);
$bannerLink = getBannerLink($postModel['banner']);

if ($items->updatePostAmount()) {

    $e = [
        "id" => $postModel['id'],
        "text" => $postModel['text'],
        "banner" => $postModel['banner'],
        "banner_link" => $bannerLink
    ];

    echo json_encode($e);

}