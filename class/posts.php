<?php

class Posts
{
    // AMOUNT MIN LIMIT
    private const AMOUNT_LIMIT = 1;

    // Connection
    private $conn;

    // Table
    private $db_table = "posts";

    // Columns
    public $id;
    public $text;
    public $price;
    public $amount;
    public $banner;

    // Db connection

    /**
     * Posts constructor.
     * @param $db
     */
    public function __construct($db)
    {
        $this->conn = $db;
    }

    // GET ONE

    /**
     * @return mixed
     */
    public function getPost()
    {

        $sqlQuery = "SELECT * FROM 
                        " . $this->db_table . " 
                    WHERE price = (SELECT MAX(price) FROM 
                        " . $this->db_table . " 
                    WHERE amount >=
                        " . self::AMOUNT_LIMIT . ") ";

        $stmt = $this->conn->prepare($sqlQuery);

        $stmt->execute();
        return $stmt;
    }

    // CREATE

    /**
     * @return bool
     */
    public function createPost()
    {
        $sqlQuery = "INSERT INTO
                        " . $this->db_table . "
                    SET
                        text = :text, 
                        price = :price, 
                        amount = :amount, 
                        banner = :banner";

        $stmt = $this->conn->prepare($sqlQuery);

        // sanitize
        $this->text = $this->getHtmlSpecialChars($this->text);
        $this->price = $this->getHtmlSpecialChars($this->price);
        $this->amount = $this->getHtmlSpecialChars($this->amount);
        $this->banner = $this->getHtmlSpecialChars($this->banner);

        // bind data
        $stmt->bindParam(":text", $this->text);
        $stmt->bindParam(":price", $this->price);
        $stmt->bindParam(":amount", $this->amount);
        $stmt->bindParam(":banner", $this->banner);

        if ($stmt->execute()) {
            return true;
        }

        return false;
    }


    // UPDATE

    /**
     * @return bool
     */
    public function updatePost()
    {
        $sqlQuery = "UPDATE
                        " . $this->db_table . "
                    SET
                        text = :text, 
                        price = :price, 
                        amount = :amount, 
                        banner = :banner
                    WHERE 
                        id = :id";

        $stmt = $this->conn->prepare($sqlQuery);

        $this->text = $this->getHtmlSpecialChars($this->text);
        $this->price = $this->getHtmlSpecialChars($this->price);
        $this->amount = $this->getHtmlSpecialChars($this->amount);
        $this->banner = $this->getHtmlSpecialChars($this->banner);
        $this->id = $this->getHtmlSpecialChars($this->id);

        // bind data
        $stmt->bindParam(":text", $this->text);
        $stmt->bindParam(":price", $this->price);
        $stmt->bindParam(":amount", $this->amount);
        $stmt->bindParam(":banner", $this->banner);
        $stmt->bindParam(":id", $this->id);

        if ($stmt->execute()) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function updatePostAmount()
    {
        $sqlQuery = "UPDATE
                        " . $this->db_table . "
                    SET
                        amount = :amount 
                    WHERE 
                        id = :id";

        $stmt = $this->conn->prepare($sqlQuery);


        $this->amount = $this->getHtmlSpecialChars($this->amount);
        $this->id = $this->getHtmlSpecialChars($this->id);

        $stmt->bindParam(":amount", $this->amount);
        $stmt->bindParam(":id", $this->id);

        if ($stmt->execute()) {
            return true;
        }

        return false;
    }

    /**
     * @param $col
     * @return string
     */
    private function getHtmlSpecialChars($col)
    {

        return htmlspecialchars(strip_tags($col));

    }
}